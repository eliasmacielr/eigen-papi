#include "bcg.h"

void bcg(SparseMatrix< double, RowMajor > &A, Matrix< double, Dynamic, Dynamic, RowMajor > &X,
         VectorXd &b, const double epsilon, int &k, const int step)
{
  int m = X.cols();
  Matrix< double, Dynamic, Dynamic, RowMajor > R, P, Q, W,
    Lambda, Psi,
    rho, rho_old;
  VectorXd r,
    e = VectorXd::Constant(m,1),
    xi;
  double xi_f,
    threshold;

  int kmax = k;

  R = b * e.transpose() - A * X;
  rho = R.transpose() * R;
  k = 1;

  threshold = epsilon * b.norm();
  while (k < kmax)
    {
      if (k % step == 0)
        {
          ColPivHouseholderQR< Matrix< double, Dynamic, Dynamic, RowMajor > > QR_W(R.rows(),R.cols());
          QR_W.compute(R);
          W = QR_W.matrixR().template triangularView< Upper >();
          xi = (W.transpose()*W).lu().solve(e);
          xi_f = 1 / (e.transpose() * xi);
          r = xi_f * R * xi;
          if (r.norm() <= threshold)
            {
              X.col(0) = xi_f * X * xi;
              return;
            }
        }
      if (k == 1)
        {
          P = R;
        }
      else
        {
          Psi = rho_old.lu().solve(rho);
          P = R + P * Psi;
        }

      Q = A * P;

      Lambda = (P.transpose()*Q).lu().solve(rho);
      X = X + P * Lambda;
      R = R - Q * Lambda;

      rho_old = rho;
      rho = R.transpose() * R;

      ++k;
    }

  ColPivHouseholderQR< Matrix< double, Dynamic, Dynamic, RowMajor > > QR_W(R.rows(),R.cols());
  QR_W.compute(R);
  W = QR_W.matrixR().template triangularView< Upper >();
  xi = (W.transpose()*W).lu().solve(e);
  xi_f = 1 / (e.transpose() * xi);
  X.col(0) = xi_f * X * xi;
  r = xi_f * R * xi;
}
