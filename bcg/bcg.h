#ifndef BCG_H
#define BCG_H

#include <cmath>

#include <Eigen/Sparse>
#include <Eigen/LU>
#include <Eigen/QR>

using namespace Eigen;

void bcg(SparseMatrix< double, RowMajor > &A, Matrix< double, Dynamic, Dynamic, RowMajor > &X,
         VectorXd &b, const double epsilon, int &k, const int step);

#endif
