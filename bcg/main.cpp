#include <cstdio>
#include <cmath>

#include <Eigen/Sparse>
#include <papi.h>

#include "mmio.h"     /* matrix market file IO */

#include "bcg.h"      /* bgc function */
#include "fill.h"     /* util to set the elements of the matrix A */
#include "opt.h"      /* cli options and arguments */
#include "mtx_proc.h" /* matrix market file processing */
#include "tfail.h"    /* test_fail function */
#include "stats.h"    /* stats function - print statistics */

using namespace Eigen;

static char *file_name = "main";


main(int argc, char **argv)
{
  int n, nz,
    m;

  FILE *f_A;

  /* PAPI - variables */
  float real_time,  /* total execution time */
    proc_time,      /* time spent on cpu */
    mflops;         /* floating point operations per second (in millions) */
  long long flpins; /* floating point instructions */
  int retval;       /* returned value from PAPI functions calls */

  /* Process cli arguments */
  struct arguments args;
  cli_opts(argc, argv, &args); f_A = fopen(args.A, "r");
  /*-------------------*/

  /* Process mtx file */
  mtx_proc(f_A, n, nz); m = args.m;
  /*-------------------*/

  SparseMatrix< double, RowMajor > A(n,n);
  Matrix< double, Dynamic, Dynamic, RowMajor > X;
  VectorXd x, b;

  fill_A(A, nz, f_A); fclose(f_A);
  b = A * VectorXd::Constant(n,1/sqrt(n));
  X = Matrix< double, Dynamic, Dynamic, RowMajor >::Random(n,m);

  int iter = (args.imax == 0 ? n : 100);
  
  /* Setup PAPI library and begin collecting data from the counters */
  if ((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);

  bcg(A, X, b, args.tol, iter, args.step);

  if ((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);

  stats((A*X.col(0)-b).norm()/b.norm(), iter, m, args.step, real_time, Eigen::nbThreads());

  /* Hardware event counting code taken from 
     Eijkhout, V. Introduction to High Performance Scientific Computing. 2014
     Chapter 37 - Codes
  */

  PAPI_shutdown();

  exit(0);
}
