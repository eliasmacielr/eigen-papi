#include "mtx_proc.h"

void mtx_proc(FILE *f, int &n, int &nz)
{
  int ret_code;
  MM_typecode matcode;

  if (mm_read_banner(f, &matcode) != 0)
    {
      printf("Could not process Matrix Market banner.\n");
      exit(1);
    }

  /* only sparse, symmetric matrices */
  if (!mm_is_sparse(matcode) || !mm_is_symmetric(matcode))
    {
      printf("Sorry, this application does not support ");
      printf("Matrix Market type: [%s]\n", mm_typecode_to_str(matcode));
      exit(1);
    }

  if ((ret_code = mm_read_mtx_crd_size(f, &n, &n, &nz)) != 0)
    exit(1);
}
