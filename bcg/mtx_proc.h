#ifndef MTX_PROC_H
#define MTX_PROC_H

#include <cstdio>
#include <cstdlib>

#include "mmio.h"

/* This function process the .mtx file containing the matrix entries.
 */
void mtx_proc(FILE *, int &, int &);

#endif
