#ifndef OPT_H
#define OPT_H

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

/* getopt : Parsing program options 
 * http://www.gnu.org/software/libc/manual/html_node/Getopt.html
 */
#include <getopt.h>

/* Structure to store the values of the arguments accepted by the program.
 */
struct arguments {
  char *A;
  int m;
  double tol;
  int imax;
  int step;
  char *output_file;
};

/* Procedure to process the options and arguments passed to the program 
 * invocation.
 * The first two parameters are the same passed to `main' function, the third 
 * is a pointer to store the result of the options and arguments processing.
 */
void cli_opts(int, char **, struct arguments *);

#endif
