#ifndef TFAIL_H
#define TFAIL_H

#include <cstdlib>
#include <cstdio>   /* printf, sprintf */
#include <cerrno>   /* perror */
#include <cstring>  /* memset */

#include <papi.h>

/*
 * Static function that is executed in case that the test performed results in
 * a failure.
 */
static void test_fail(char *file, int line, char *call, int retval)
{
  printf("%s\tFAILED\nLine # %d\n", file, line);
  
  if(retval == PAPI_ESYS)
    {
      char buf[128];
      memset(buf, '\0', sizeof(buf));
      sprintf(buf, "System error in %s:", call);
      perror(buf);
    }
  else if(retval > 0)
    {
      printf("Error calculating: %s\n", call);
    }
  else
    {
      printf("Error in %s: %s\n", call, PAPI_strerror(retval));
    }

  printf("\n");
  exit(1);
}

#endif
