#include "cg.h"

void cg(Eigen::SparseMatrix< double, Eigen::RowMajor > &A, Eigen::VectorXd &x,
       Eigen::VectorXd &b, const double tol, int &iter)
{
  Eigen::VectorXd r, p, w;
  double alpha, beta,
    rho, rho_old,
    threshold;

  int imax = iter;

  r = b - A * x;
  rho = r.squaredNorm();
  iter = 1;

  threshold = tol * b.norm();
  while (sqrt(rho) > threshold && iter < imax)
    {
      if (iter == 1)
        {
          p = r;
        }
      else
        {
          beta = rho / rho_old;
          p = r + beta * p;
        }

      w = A * p;

      alpha = rho / (p.transpose() * w);
      x += alpha * p;
      r -= alpha * w;

      rho_old = rho;
      rho = r.squaredNorm();

      ++iter;
    }
}
