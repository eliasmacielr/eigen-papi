#ifndef CG_H
#define CG_H

#include <Eigen/Sparse>

void cg(Eigen::SparseMatrix< double, Eigen::RowMajor > &A, Eigen::VectorXd &x,
        Eigen::VectorXd &b, const double tol, int &iter);

#endif
