#include "fill.h"

/* The most efficient way to fill a sparse matrix is using a std::vector of
   Eigen::Triplet, as stated in
   https://eigen.tuxfamily.org/dox/group__TutorialSparse.html
 */
void fill_A(Eigen::SparseMatrix< double, Eigen::RowMajor > &A, int nnz, FILE *f)
{
  typedef Eigen::Triplet<double> T; /* T(row, column, value) */

  int r, c;
  double val;
  std::vector<T> aa;

  for (size_t i = 0; i < nnz; ++i)
    {
      fscanf(f, "%d %d %lg\n", &r, &c, &val);
      aa.push_back(T(r-1, c-1, val));
      if (r != c) { aa.push_back(T(c-1, r-1, val)); }
    }

  A.setFromTriplets(aa.begin(), aa.end());
}
