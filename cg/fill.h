#ifndef FILL_H
#define FILL_H

#include <cstdio>

#include <vector>

#include <Eigen/Sparse>

/* This subprogram assigns elements to the n-order matrix passed by reference 
   in order to fill a sparse matrix.
 */
void fill_A(Eigen::SparseMatrix< double, Eigen::RowMajor > &, int, FILE *);

#endif
