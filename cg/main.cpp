#include <cstdio>
#include <cmath>

#include <Eigen/Sparse>
#include <papi.h>

#include "mmio.h"     /* matrix market file IO */

#include "cg.h"
#include "fill.h"     /* util to set the elements of the matrix A */
#include "opt.h"      /* cli options and arguments */
#include "mtx_proc.h" /* matrix market file processing */
#include "tfail.h"    /* test_fail function */
#include "stats.h"    /* stats function - print statistics */


static char *file_name = "main";

main(int argc, char **argv)
{
  int n, nz;

  FILE *f_A;

  /* PAPI - variables */
  float real_time,  /* total execution time */
    proc_time,      /* time spent on cpu */
    mflops;         /* floating point operations per second (in millions) */
  long long flpins; /* floating point instructions */
  int retval;       /* returned value from PAPI functions calls */

  /* Process cli arguments */
  struct arguments args;
  cli_opts(argc, argv, &args); f_A = fopen(args.A, "r");
  /*-------------------*/

  /* Process mtx file */
  mtx_proc(f_A, n, nz);
  /*-------------------*/

  Eigen::SparseMatrix< double, Eigen::RowMajor > A(n,n);
  Eigen::VectorXd x, b;

  fill_A(A, nz, f_A); fclose(f_A);
  b = A * Eigen::VectorXd::Constant(n,1/sqrt(n));
  x = Eigen::VectorXd::Random(n);

  int iter = (args.imax == 0 ? n : 100);

  /* Setup PAPI library and begin collecting data from the counters */
  if ((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);

  cg(A, x, b, args.tol, iter);

  if ((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);

  stats((A*x-b).norm()/b.norm(), iter, real_time, Eigen::nbThreads());

  /* Hardware event counting code taken from 
     Eijkhout, V. Introduction to High Performance Scientific Computing. 2014
     Chapter 37 - Codes
  */

  PAPI_shutdown();

  exit(0);
}
