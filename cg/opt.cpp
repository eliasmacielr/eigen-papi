#include "opt.h"

void cli_opts(int argc, char **argv, struct arguments *a)
{
  /* Default values */
  a->A = NULL;
  a->tol = 1e-6;
  a->imax = 0;
  a->output_file = NULL;
  
  int c;

  while(1)
    {
      static struct option long_options[] =
        {
          /* These options don't set a flag. */
          { "A",           required_argument, 0, 'A' },
          { "tol",         required_argument, 0, 't' },
          { "imax",        required_argument, 0, 'i' },
          { "output_file", required_argument, 0, 'o' },
          { 0, 0, 0, 0 }
        };

      /* getopt_long stores the option index here. */
      int option_index = 0;

      c = getopt_long(argc, argv, "A:t:i:o",
                      long_options, &option_index);

      if(c == -1)
        break;
      
      switch(c)
        {
        case 'A':
          a->A = optarg;
          break;

        case 't':
          a->tol = atof(optarg);
          break;

        case 'i':
          a->imax = atoi(optarg);
          break;

        case 'o':
          a->output_file = optarg;
          freopen(optarg, "w", stdout); /* sets the specified file as stdout */
          break;

        case '?':
          /* getopt_long already printed an error message. */
          exit(1);
        default:
          abort();
        }
    }

  if (a->A == NULL)
    {
      printf("Usage: %s -A [matrix-market-filename]\n", argv[0]);
      exit(1);
    }
}
