#include "stats.h"

void stats(double rel_res, int iter, float real_time, int n_threads)
{
  std::cout
    << "Relative residual [norm(Ax-b)/norm(b)]: " << rel_res << std::endl
    << "Iterations: " << iter << std::endl
    << "Real time: " << real_time << "s" << std::endl
    << "Number of threads: " << n_threads << std::endl;
}
