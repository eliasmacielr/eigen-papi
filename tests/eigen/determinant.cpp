#include <iostream>

#include <Eigen/LU>

int main()
{
  Eigen::Matrix3i A;
  A <<
    1, 0, 0,
    0, 1, 0,
    0, 0, 1;
  std::cout << "A = " << std::endl << A << std::endl;
  std::cout << "Coefficients on the main diagonal of A:" << std::endl
            << A.diagonal() << std::endl;
  std::cout << "The determinant is: " << A.determinant() << std::endl;

  return 0;
}
