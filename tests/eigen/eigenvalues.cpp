#include <iostream>

#include <Eigen/Eigenvalues>

using namespace std;
using namespace Eigen;

int main()
{
  // Matrix
  Matrix2d A;
  A <<
    .616555556, .615444444,
    .615444444, .716555556;

  // Eigenvectors
  Vector2d x_1, x_2;
  x_1 <<
    -.735178656,
    .677873399;
  x_2 <<
    -.677873399,
    -.735178656;

  // Eigenvalues
  VectorXcd eivals = A.eigenvalues();

  // Show results
  cout << "A = " << endl
       << A << endl;

  cout << "Eigenvalues:" << endl;
  cout << "λ_1 = " << eivals(0) << endl;
  cout << "λ_2 = " << eivals(1) << endl;

  cout << "Eigenvectors:" << endl;
  cout << "x_1 = " << endl
       << x_1 << endl;

  cout << "x_2 = " << endl
       << x_2 << endl;

  cout << "Results:" << endl;
  cout << "Ax_1 =" << endl
       << A * x_1 << endl;

  cout << "λ_1x_1 =" << endl
       << eivals(0) * x_1 << endl;

  cout << "Ax_2 =" << endl
       << A * x_2 << endl;

  cout << "λ_1x_2 =" << endl
       << eivals(1) * x_2 << endl;

  return 0;
}
