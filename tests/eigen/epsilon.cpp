#include <iostream>
#include <limits>

#include <Eigen/Eigen>

using namespace Eigen;

main()
{
  std::cout << "Eigen::NumTraits<double>::epsilon() = "
            << NumTraits<double>::epsilon()
            << std::endl
            << "std::numeric_limits<double>::epsilon() = "
            << std::numeric_limits<double>::epsilon()
            << std::endl;
}
