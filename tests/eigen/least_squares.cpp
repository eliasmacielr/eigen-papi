#include <iostream>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

int main()
{
  MatrixXf A(3, 2);
  A <<
    1, 1,
    2, 3,
    3, 1;
  
  cout << "Here is the matrix A:\n" << A << endl;

  Vector3f b(3);
  b <<
    1,
    1,
    1;
  
  cout << "Here is the right hand side b:\n" << b << endl;
  
  cout << "The least-squares solution is:\n"
       << A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b) << endl;
}
