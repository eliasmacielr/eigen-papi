#include <cstdlib>
#include <cstdio>

#include <iostream>
#include <vector>

#include <Eigen/IterativeLinearSolvers>


typedef Eigen::SparseMatrix<double> SpMat;

void fill_A(SpMat &);

main(int argc, char *argv[])
{
  int n = (atoi(argv[1]) > 0 ? atoi(argv[1]) : 1000); // 1000 as default
  SpMat A(n,n);

  fill_A(A);
  
  std::cout << "A nnz = " << A.nonZeros() << std::endl;
}

void fill_A(SpMat &A)
{
  typedef Eigen::Triplet<double> T;

  int n = A.rows();
  std::vector<T> aa;

  double dx2 = double(n)*double(n);

  for(int i = 0; i < n; ++i)
    {
      if(i > 0) aa.push_back(T(i, i-1, dx2));
      aa.push_back(T(i, i, -2 * dx2));
      if(i < n-1) aa.push_back(T(i, i+1, dx2));
    }

  A.setFromTriplets(aa.begin(), aa.end());
}
