#include <iostream>

#include <Eigen/LU>

main()
{
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(50,50);
  Eigen::MatrixXd B = Eigen::MatrixXd::Random(50,60);

  Eigen::MatrixXd X = A.lu().solve(B);
  
  std::cout << "Here is the (unique) solution X to the equation AX = B:"
            << std::endl << X << std::endl << std::endl;

  std::cout << "Relative error: " << (A*X-B).norm() / B.norm() << std::endl;
}
