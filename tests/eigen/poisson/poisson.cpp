#include <iostream>
#include <vector>

#include <Eigen/IterativeLinearSolvers>

using std::cout;
using std::endl;

using Eigen::VectorXi;
using Eigen::VectorXd;
using Eigen::SparseMatrix;
using Eigen::ConjugateGradient;

const Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "];");

typedef SparseMatrix<double> SpMat;

void fill_A(SpMat& A)
{
  typedef Eigen::Triplet<double> T;

  int n = A.rows();

  std::vector<T> aa; // list of non-zeros coefficients

  double dx2 = double(n) * double(n);

  for (int i = 0; i < n; i++)
    {
      if(i>0) aa.push_back( T(i,i-1, dx2 ));
      aa.push_back( T(i,i, -2 * dx2 ) );
      if(i<n-1) aa.push_back( T(i,i+1, dx2 ));
    }

  A.setFromTriplets(aa.begin(), aa.end());

  std::cout << " Filled A " << std::endl;
}
 
void fill_b(VectorXd &b)
{
  int n = b.size();
  for (int i = 0; i < n; i++)
    {
      double x = double(i+1) / double(n+1);
      b(i)= std::sin(2*M_PI*x);
      std::cout << x << " " << b(i) << std::endl;
    }
}
 
void solution(VectorXd &b)
{
  int n = b.size();
  for (int i = 0; i < n; i++)
    {
      double x = double(i+1) / double(n+1);
      b(i) = -std::sin(2*M_PI*x) / (4*M_PI*M_PI);
    }
}
 
int main()
{
  int n = 100;
  VectorXd x(n), b(n);
  SpMat A(n,n);

  // fill A
  fill_A(A);

  // fill b
  fill_b(b);

  std::cout << "b = " << b.format(OctaveFmt) << std::endl;

  VectorXd x0(n);
  solution(x0);

  std::cout << "x0 = " << x0.format(OctaveFmt) << std::endl;

  VectorXd bb = A * x0;

  std::cout << "bb = " << bb.format(OctaveFmt) << std::endl;

  // solve Ax = b
  // Eigen::BiCGSTAB<SpMat> solver;
  ConjugateGradient<SpMat> solver;

  solver.compute(A);
  if (solver.info() != Eigen::Success)
    {
      // decomposition failed
      std::cout << "Failure decomposing\n";
      return 1;
    }

  // x = solver.solveWithGuess(b, x0);
  x = solver.solve(b);
  if (solver.info() != Eigen::Success)
    {
      // solving failed
      std::cout << "Failure solving\n";
      return 1;
    }

  // solve for another right hand side:
  // x1 = solver.solve(b1);

  std::cout << "#iterations: " << solver.iterations() << std::endl;
  std::cout << "estimated error: " << solver.error() << std::endl;

  std::cout << "x = " << x.format(OctaveFmt) << std::endl;

  // update b, and solve again
  //* x = cg.solve(b);

  return 0;
}
