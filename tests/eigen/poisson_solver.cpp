#include <iostream>
#include <vector>

#include <Eigen/IterativeLinearSolvers>

#include <papi.h>

#include "tfail.h" /* test_fail function definition */

static char *file_name = "poisson_solver";

using Eigen::VectorXi;
using Eigen::VectorXd;
using Eigen::SparseMatrix;
using Eigen::ConjugateGradient;
 
using std::cout;
using std::endl;
 
const Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "];");
 
typedef SparseMatrix<double> SpMat;
 
void fill_A(SpMat &A) { 
  typedef Eigen::Triplet<double> T;
  
  std::cout << " Filling A " << std::endl;
  int n = A.rows();
  std::vector<T> aa; // list of non-zeros coefficients
  double dx2 = double(n)*double(n);
  
  for (int i = 0; i < n; i++) {
    if (i > 0) aa.push_back(T(i,i-1, dx2));
    aa.push_back(T(i,i, -2 * dx2));
    if (i < n-1) aa.push_back(T(i,i+1, dx2));
  }

  A.setFromTriplets(aa.begin(), aa.end());

  std::cout << " Filled A " << std::endl;
  return;
}
 
void fill_b(VectorXd &b) {
  int n = b.size();
  for (int i = 0; i < n; i++) {
    double x = double(i+1)/double(n+1);
    b(i)= std::sin(2*M_PI*x);
    //std::cout << x << " " << b(i) << std::endl;
  }
  return;
}
 
void solution(VectorXd &b) {
  int n = b.size();
  for(int i = 0; i < n; i++) {
    double x = double(i+1)/double(n+1);

    b(i)= -std::sin(2*M_PI*x) / (4*M_PI*M_PI);
  }
  return;
}
 
int main() {

  int nb_threads = Eigen::nbThreads();

  std::cout << "Number of threads used: " << nb_threads << std::endl
            << std::endl;

  float real_time,  /* total execution time */
    proc_time,      /* time spent on cpu */
    mflops;         /* millions of floating point operations per second */
  long long flpins; /* floating point instructions */
  int retval;       /* returned value from PAPI functions calls */
  
  int n = 100;
  VectorXd x(n), b(n);
  SpMat A(n,n);
 
  fill_A(A);
 
  fill_b(b);
 
  //std::cout << "b = " << b.format(OctaveFmt) << std::endl;
 
  VectorXd x0(n);
  solution(x0);
 
  //std::cout << "x0 = " << x0.format(OctaveFmt) << std::endl;
 
  VectorXd bb = A * x0;
 
  //std::cout << "bb= " << bb.format(OctaveFmt) << std::endl;
 
  // solve Ax = b
  Eigen::BiCGSTAB<SpMat> solver;
  //ConjugateGradient<SpMat> solver;

  /* Setup PAPI library and begin collecting data from the counters */
  if((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);
  
  solver.compute(A);

  /* Collect the data into the variables passed in */
  if((retval = PAPI_flops(&real_time, &proc_time, &flpins, &mflops)) < PAPI_OK)
    test_fail(__FILE__, __LINE__, file_name, retval);
  
  if (solver.info() != Eigen::Success) {
    // decomposition failed
    std::cout << "Failure decomposing\n";
    return 1;
  }
 
  x = solver.solveWithGuess(b,x0);
  if (solver.info() != Eigen::Success) {
    // solving failed
    std::cout << "Failure solving\n";
    return 1;
  }
  // solve for another right hand side:
  // x1 = solver.solve(b1);
 
  std::cout << "#iterations: " << solver.iterations() << std::endl;
  std::cout << "estimated error: " << solver.error() << std::endl << std::endl;

  std::cout << "Real time:\t" << real_time << "s" << std::endl
            << "Proc time:\t" << proc_time << "s" << std::endl
            << "Total flpins:\t\t" << flpins << std::endl
            << "MFLOPS:\t\t\t" << mflops << std::endl;
  //std::cout << "x = " << x.format(OctaveFmt) << std::endl;
 
  // update b, and solve again
  //* x = cg.solve(b);

  return 0;
}
