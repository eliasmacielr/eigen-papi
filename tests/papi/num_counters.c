#include <stdio.h>
#include <stdlib.h>
#include <papi.h>
#include "herror.h"

int main()
{
  int num_hwcntrs;

  num_hwcntrs = PAPI_num_counters();
  /* The installation does not support PAPI */
  if(num_hwcntrs < 0)
    handle_error(1);

  /* The installation supports PAPI, but has no counters */
  else if(num_hwcntrs == 0)
    fprintf(stderr, "Info:: This machine does not provide hardware counters.\n");

  else
    printf("Hardware counters: %d\n", num_hwcntrs);

  return 0;
}
