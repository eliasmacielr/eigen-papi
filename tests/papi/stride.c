#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define NRUNS 10
#define MAX_STRIDES 8
#define L1WORDS 8096

int main()
{
  size_t stride;
  double *array = malloc(L1WORDS * sizeof(double));

  clock_t t;
  size_t i,j,n;
  for (stride = 1; stride < MAX_STRIDES; ++stride)
    {
      t = clock();
      for (j = 0, n = 0; j < L1WORDS; ++i, n+=stride)
        {
          *(array + n) = 2.3 * *(array + n) + 1.2;
        }
      t = clock() - t;
      printf("strides: %d\ntime: %f\n\n", stride, (float)t/CLOCKS_PER_SEC);
    }

  return 0;
}
